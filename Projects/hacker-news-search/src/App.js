import React from 'react';

import './App.css';
import Navbar from './components/Navbar';
import News from './components/News';
import SortBy from './components/SortBy';
import PreLoader from './components/PreLoader';
import Error from './components/Error';



class App extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      news: [],
      preloader: true,
      error: false,
      sort: 'search',
      type: 'story'
    }
  }

  fetchNews = (event) => {
    if (event) {

      if (event.target.name === 'type')
        this.setState({
          type: event.target.value

        })
      else {
        this.setState({
          sort: event.target.value
        })
      }
    }

    fetch(`https://hn.algolia.com/api/v1/${this.state.sort}?tags=${this.state.type}`)
      .then(res => res.json())
      .then((data) => {
        this.setState({
          news: data.hits,
          preloader: false
        })
      })
      .catch((err) => {
        console.error(err);
        this.setState({
          error: true
        })
      })
  }
  componentDidMount() {
    this.fetchNews();
  }
  render() {
    return (
      <div className="App" >
        {
          this.state.error ? <>
            <Navbar />
            <Error />
          </> : <>
            <Navbar />
            <SortBy fetchNews={this.fetchNews.bind(this)} />
            {
              this.state.preloader ? <PreLoader /> : <></>
            }
            <News news={this.state.news} key={1} />
          </>
        }


      </div>
    );
  }
}

export default App;