import React from 'react'
import './Navbar.css'
import gear from '../images/gear.svg'
import logo from '../images/logo.webp'
import algolia from '../images/logo-algolia.svg'

class Navbar extends React.Component {
    render() {
        return (
            <nav>
                <img src={logo} className='logo' alt='Hacker News'/>
                <div className='logo-name'>
                    <p >Search</p> <p>Hacker News</p>
                </div>
                <div className='searchBar'>
                    <i className="fa-solid fa-magnifying-glass"></i>
                    <input type="search" placeholder='Search stories by title, url or author' />
                    <div className="algolia-div">
                        <p className='searched-by'>Searched by</p>
                        <img className='algolia-image' src={algolia} alt='alogia'/>
                    </div>
                </div>
                <div className='setting'> <img src={gear} alt='settings'/> <p className='setting-label'>Setting</p></div>
            </nav>
        )
    }
}

export default Navbar;
