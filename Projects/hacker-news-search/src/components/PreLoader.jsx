import React, { Component } from 'react';
import './PreLoader.css';

export default class PreLoader extends Component {
    render() {
        return (
            <div className="preloader-div">
                <div className="lds-spinner">
                    <div></div>
                    <div></div>
                    <div></div>
                    <div></div>
                    <div></div>
                    <div></div>
                    <div></div>
                    <div></div>
                    <div></div>
                    <div></div>
                    <div></div>
                    <div></div>
                </div>
            </div>
        )
    }
}
