import React from 'react'
import './News.css'
import moment from 'moment'

export default class News extends React.Component {
  render(){
    return(
      this.props.news.map((news)=>{
        return(
            news.title ?
            <div className="news">
              <div className="title-url">
                <a className="title" href={`https://news.ycombinator.com/item?id=${news.objectID}`}>{news.title}</a>
                <a className="url" href={news.url}>{news.url}</a>
              </div>
              <div className="news-reaction">
                <a className="title" href={`https://news.ycombinator.com/item?id=${news.objectID}`}> {news.points} points | </a>
                <a className="title" href={`https://news.ycombinator.com/user?id= ${news.author}`}>{news.author} | </a>
                <a className="title" href={`https://news.ycombinator.com/item?id=${news.objectID}`}>{moment(news.created_at).fromNow()} | </a>
                <a className="title" href={`https://news.ycombinator.com/item?id=${news.objectID}`}>{news.num_comments} comments</a>
              </div>
            </div>
            :
            <></>
        )
      })
    )
  }
}
