import React, { Component } from 'react'
import './Error.css'

export default class Error extends Component {
  render() {
    return (
      <div className='error-message'>
        <h1>We are facing some error</h1>
      </div>
    )
  }
}
