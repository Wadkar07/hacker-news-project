import React from 'react';
import './SortBy.css'
import share from '../images/share.svg'

export default class SortBy extends React.Component {
    // onUpdate(event){
    //     this.props.fetchNews(event.target.name,event.target.value)
    // }

    getFetchNews = (event) => {
        this.props.fetchNews(event);
    }

    render() {
        return (
            <div className="row-two">
                <div className="sort-by">
                    <span className='label'> Search </span>
                    <span>
                        <select name="type" onChange={this.getFetchNews} defaultValue='stories'>
                            <option value="story" className="filter-option">Stories</option>
                            <option value="comment" className="filter-option">Comments</option>
                        </select>
                    </span>
                    <span className='label'> by </span>
                    <span>
                        <select name="sort" onChange={this.getFetchNews} defaultValue='popularity'>
                            <option value="search" className="option">Popularity</option>
                            <option value="search_by_date" className="option">Date</option>

                        </select>
                    </span>
                </div>
                <div className="response-details">
                    <p className='result-count'>3,24,19062 results (0.003 seconds)</p>
                    <img src={share} alt='share' />
                </div>
            </div>
        )
    }
}